'use strict';

const _ = require('lodash');

/**
* Secret.js controller
*
* @description: A set of functions called "actions" for managing `Secret`.
*/

module.exports = {

    /**
    * Retrieve secret records.
    *
    * @return {Object|Array}
    */

    find: async (ctx) => {
        ctx.query.user = { _id: ctx.state.user._id };
        let data = await strapi.services.secret.fetchAll(ctx.query);
        return _.map(data, value => {
            return _.pick(value, ['create', 'expire', 'comment']);
        });
    },

    /**
    * Retrieve a secret record.
    *
    * @return {Object}
    */

    findOne: async (ctx) => {
        if (!ctx.params.secret.match(/^[0-9a-zA-Z]{16}$/)) {
            return ctx.notFound();
        }

        let secret = await strapi.services.secret.fetch(ctx.params);
        if (!secret) {
            return ctx.notFound();
        }
        return _.pick(secret, ['create', 'expire', 'comment']);
    },

    /**
    * Count secret records.
    *
    * @return {Number}
    */

    count: async (ctx) => {
        ctx.query.user = ctx.state.user._id;
        return strapi.services.secret.count(ctx.query);
    },

    /**
    * Create a/an secret record.
    *
    * @return {Object}
    */

    create: async (ctx) => {
        ctx.request.body.user = ctx.state.user._id;
        let data = await strapi.services.secret.add(ctx.request.body);
        return _.pick(data, ['create', 'expire', 'comment', 'secret']);
    },

    /**
    * Update a/an secret record.
    *
    * @return {Object}
    */

    update: async (ctx, next) => {
        if (!ctx.params.secret.match(/^[0-9a-zA-Z]{16}$/)) {
            return ctx.notFound();
        }
        //Check can edit
        let secret = await strapi.services.secret.fetch(ctx.params);
        if (!secret) {
            return ctx.notFound();
        }
        if (!ctx.state.user._id.equals(secret.user)) {
            return ctx.forbidden();
        }
        //Edit
        secret = await strapi.services.secret.edit(ctx.params, ctx.request.body);
        return _.pick(secret, ['create', 'expire', 'comment']);
    },

    /**
    * Destroy a/an secret record.
    *
    * @return {Object}
    */

    destroy: async (ctx, next) => {
        if (!ctx.params.secret.match(/^[0-9a-zA-Z]{16}$/)) {
            return ctx.notFound();
        }
        //Check can delete
        let secret = await strapi.services.secret.fetch(ctx.params);
        if (!secret) {
            return ctx.notFound();
        }
        if (!ctx.state.user._id.equals(secret.user)) {
            return ctx.forbidden();
        }
        //Delete the secret
        secret = await strapi.services.secret.remove(ctx.params);
        return _.pick(secret, ['create', 'expire', 'comment'])
    }
};
