'use strict'
const _ = require('lodash');

module.exports = async (ctx, next) => {
    if (ctx.request.header.secret) {
        let secret = await strapi.services.secret.fetch(ctx.params);
        if (!secret) {
            return ctx.forbidden();
        }
        ctx.state.user = await strapi.plugins['users-permissions'].services.user.fetch({ _id: secret.user });
        return await next();
    }
    return await next();
};
