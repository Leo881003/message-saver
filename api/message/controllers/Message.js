'use strict';

/**
* Message.js controller
*
* @description: A set of functions called "actions" for managing `Message`.
*/

const _ = require('lodash');
const mongoose = require('mongoose');

module.exports = {

    /**
    * Retrieve message records.
    *
    * @return {Object|Array}
    */

    find: async (ctx) => {
        ctx.query.owner = { _id: ctx.state.user._id };
        let message;
        if (ctx.query._q) {
            message = await strapi.services.message.search(ctx.query);
        } else {
            message = await strapi.services.message.fetchAll(ctx.query);
        }

        return _.map(message, value => {
            return _.pick(value, ['name', 'content', 'create']);
        });
    },

    /**
    * Retrieve a message record.
    *
    * @return {Object}
    */

    findOne: async (ctx) => {
        if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
            return ctx.notFound();
        }

        let message = await strapi.services.message.fetch(ctx.params);
        if (!ctx.state.user._id.equals(message.owner)) {
            return ctx.forbidden();
        }
        return _.pick(message, ['name', 'content', 'create']);
    },

    /**
    * Count message records.
    *
    * @return {Number}
    */

    count: async (ctx) => {
        ctx.query.owner = ctx.state.user._id;
        return strapi.services.message.count(ctx.query);
    },

    /**
    * Create a/an message record.
    *
    * @return {Object}
    */

    create: async (ctx) => {
        ctx.request.body.owner = ctx.state.user._id;
        let message = await strapi.services.message.add(ctx.request.body);
        return _.pick(message, ['name', 'content', 'create']);
    },

    /**
    * Update a/an message record.
    *
    * @return {Object}
    */

    update: async (ctx, next) => {
        if (!ctx.params._id.match(/^[0-9a-zA-Z]{16}$/)) {
            return ctx.notFound();
        }
        //Check can edit
        let message = await strapi.services.message.fetch(ctx.params);
        if (!message) {
            return ctx.notFound();
        }
        if (!ctx.state.user._id.equals(message.user)) {
            return ctx.forbidden();
        }
        //Edit
        message = await strapi.services.message.edit(ctx.params, ctx.request.body);
        return _.pick(message, ['name', 'content', 'create']);
    },

    /**
    * Destroy a/an message record.
    *
    * @return {Object}
    */

    destroy: async (ctx, next) => {
        if (!ctx.params._id.match(/^[0-9a-zA-Z]{16}$/)) {
            return ctx.notFound();
        }
        //Check can edit
        let message = await strapi.services.message.fetch(ctx.params);
        if (!message) {
            return ctx.notFound();
        }
        if (!ctx.state.user._id.equals(message.user)) {
            return ctx.forbidden();
        }
        //Delete
        message = await strapi.services.message.remove(ctx.params);
        return _.pick(message, ['name', 'content', 'create']);
    }
};
